"use strict";

const token = process.env.WHATSAPP_TOKEN;

// Imports dependencies and set up http server
const request = require("request"),
express = require("express"),
body_parser = require("body-parser"),
axios = require("axios").default,
app = express().use(body_parser.json());
require('dotenv').config(); // creates express http server

// Dialogflow
const dialogflow = require('@google-cloud/dialogflow');
const uuid = require('uuid');

/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */
async function askDialogflow(text) {
  // A unique identifier for the given session
  const sessionId = uuid.v4();
  const projectId = process.env.GOOGLE_PROJECT_ID;

  // Create a new session
  const sessionClient = new dialogflow.SessionsClient();
  const sessionPath = sessionClient.projectAgentSessionPath(projectId, sessionId);

  // The text query request.
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        // The query to send to the dialogflow agent
        text: text,
        // The language used by the client (en-US)
        languageCode: 'en-US',
      },
    },
  };

  // Send request and log result
  const responses = await sessionClient.detectIntent(request);
  console.log('Detected intent');
  const result = responses[0].queryResult;
  console.log(`  Query: ${result.queryText}`);
  console.log(`  Response: ${result.fulfillmentText}`);
  if (result.intent) {
    console.log(`  Intent: ${result.intent.displayName}`);
  } else {
    console.log('  No intent matched.');
  }

  return result.fulfillmentText;
}


// Sets server port and logs message on success
console.log(`PORT: ${4001}`)
app.listen(4001, () => console.log("webhook is listening on 4001"));

// Accepts POST requests at /webhook endpoint
app.post("/webhook", (req, res) => {
  console.log('POST webhook');
  // Check the Incoming webhook message
  console.log(JSON.stringify(req.body, null, 2));

  // info on WhatsApp text message payload
  if (!req.body.object) {
    res.sendStatus(404);
    return;
  }
  if (
    !req.body.entry ||
    !req.body.entry[0].changes ||
    !req.body.entry[0].changes[0] ||
    !req.body.entry[0].changes[0].value.messages ||
    !req.body.entry[0].changes[0].value.messages[0]
  ) {
    res.sendStatus(200);
    return;
  }

  let phone_number_id = req.body.entry[0].changes[0].value.metadata.phone_number_id;
  let from = req.body.entry[0].changes[0].value.messages[0].from; // extract the phone number from the webhook payload
  let msg_body = req.body.entry[0].changes[0].value.messages[0].text.body; // extract the message text from the webhook payload
  
  // DIALOGFLOW
  askDialogflow(msg_body)
    .then(function (text) {
      console.log(text);

      axios({
        method: "POST", // Required, HTTP method, a string, e.g. POST, GET
        url: "https://graph.facebook.com/v13.0/" + phone_number_id + "/messages?access_token=" + process.env.WHATSAPP_TOKEN,
        data: {
          messaging_product: "whatsapp",
          to: from,
          text: { body: "Bot: " + text },
        },
        headers: { "Content-Type": "application/json" },
      })
        .then(function (response) {
          console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
          console.log(error);
        });

    });

  res.sendStatus(200);
});

// Accepts GET requests at the /webhook endpoint. You need this URL to setup webhook initially.
// info on verification request payload: https://developers.facebook.com/docs/graph-api/webhooks/getting-started#verification-requests
app.get("/webhook", (req, res) => {
  console.log('GET webhook');
  // Parse params from the webhook verification request
  let mode = req.query["hub.mode"];
  let token = req.query["hub.verify_token"];
  let challenge = req.query["hub.challenge"];

  // Check if a token and mode were sent
  if (mode && token) {
    // Check the mode and token sent are correct
    if (mode === "subscribe" && token === process.env.VERIFY_TOKEN) {
      // Respond with 200 OK and challenge token from the request
      console.log("Webhook Verified");
      res.status(200).send(challenge);
      return;
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
      return;
    }
  }
  res.sendStatus(400);
});
